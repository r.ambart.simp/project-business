# project-business



### Diagram de class:

![project-business](/home/rapha/Images/project-business.png)


### Presentation du projet :

Une personne pourra créer un groupe et inviter d'autres personnes via un lien d'invitation. A partir de là, les différents membres du groupe pourront ajouter des dépenses et voir le total actuel de celles ci (et une estimation de la fraction que chaque membre devra payer).

Une fois l'activité terminé, on passe le group en mode faire le compte (Settle group), ce qui affichera pour chaque membre la somme qu'ielle devra verser à celleux ayant fait d'avantage de dépense et permettra à ces membres en question de valider le remboursement lorsqu'un membre lui règle sa part.

##  Mise en oeuvre

Vous allez vous concentrer uniquement sur la partie API Rest (pas de front pour ce projet, sauf si vous avez très très envie). Comme dit l'idée sera de faire à terme une couche Business (voir Domain) qui contiendra toutes les règles métiers de l'application et si elle peut être complètement découplée de l'infrastructure, c'est chouette.

Pas la peine de gérer la partie authentification sur ce projet.

Pas de restriction de langage/framework/ORM pour ce projet (sachant que si vous voulez faire les choses bien ceci dit, le fait d'utiliser certains framework en gardant la couche Business/Domain découplée demandera certaines adaptations)