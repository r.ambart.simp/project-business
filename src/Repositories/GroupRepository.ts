import { getConnection, UpdateResult } from "typeorm";
import { Group } from "../Entity/Group";
import { User } from "../Entity/User";



export default class GroupRepository {
    private repo = () => getConnection().getRepository(Group)

    createGroup(users: User[]) {
        let repo = this.repo();
        return repo.save(repo.create({ members: users, isSettled: false, isValidated: false }))
    }

    addUserToGroup(groupId: number, user: User) {
        let repo = this.repo();
        return repo.findOne(groupId, { relations: ["members"] }).then(group => {
            group.members.push(user)
            return repo.save(group)
        })
    }

    getGroupById(groupId: number): Promise<Group> {
        return this.repo().findOne(groupId)
    }

    settleGroup(groupId: number): Promise<UpdateResult> {
        return this.repo().update(groupId, { isSettled: true })
    }
    
}