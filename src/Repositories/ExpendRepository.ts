import { getConnection } from "typeorm";
import { Expend } from "../Entity/Expend";
import { Group } from "../Entity/Group";
import { User } from "../Entity/User";



export default class ExpendRepository {
    private repo = () => getConnection().getRepository(Expend)

    getExpendById(expendExpendId: number) {
        return this.repo().findOne(expendExpendId, { relations: ["group"] })
    }
    addExpend(amount: number, pay: User, group: Group) {
        let repo = this.repo()
        return repo.save(repo.create({ amount: amount, pay: pay, group: group }))
    }
    
    cancelExpend(expendId: number) {
        return this.repo().update(expendId, { isCancel: true });
        // return this.repo().delete(expenseId);
    }

}