import { getConnection } from "typeorm";
import { Group } from "../Entity/Group";
import { User } from "../Entity/User";



export default class UserRepository {
    private repo = () => getConnection().getRepository(User)

    createUser(username: string): Promise<User> {
        let repo = this.repo();
        return repo.save(repo.create({ name: username }))
    }
    getUserByName(username: string): Promise<User> {
        return this.repo().findOne({ where: [{ name: username }] });
    }

    getUserById(userId: number, includeGroups: boolean = false): Promise<User> {
        return this.repo().findOne(userId, { relations: includeGroups ? ["groups"] : [] })
    }

    getUserGroups(userId: number): Promise<Group[]> {
        return this.getUserById(userId, true).then(user => {
            return user.groups
        })
    }
    
}