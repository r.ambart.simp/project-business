import express from 'express'
import GroupController from '../Controller/GroupController';
import UserController from '../Controller/UserController';
import ExpendController from '../Controller/ExpendController';

const router = express()

router.use("/group", GroupController);
router.use("/user", UserController);
router.use("/expend", ExpendController);

export default router
