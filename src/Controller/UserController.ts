import express from "express";
import UserBusiness from "../Business/UserBusiness"
import { Group } from "../Entity/Group";

const UserController = express.Router();
const userBusiness = new UserBusiness();

UserController.get("/groups", (req, res) => {

    let userId = req.body.userId;
    userBusiness.getUserGroups(userId).then((groups: Group[]) => {
        res.send(groups)

    }).catch((error) => {
        res.status(400).send(error)
    });
})

export default UserController;