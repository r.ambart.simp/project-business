import express from "express";
import GroupBusiness from "../Business/GroupBusiness"

const GroupController = express.Router();
const groupBusiness = new GroupBusiness();

GroupController.route("/").post((req, res) => {
    let users = req.body.users

        groupBusiness.createGroup(users).then(group => {
            res.send(group)
        }).catch((error) => {
        res.status(400).send(error)
    });
})

GroupController.post("/join", (req, res) => {
    let userId = req.body.userId
    let groupId = req.body.groupId
 
        groupBusiness.addUserToGroup(userId, groupId).then(group => {
            res.send(group)
        }).catch((error) => {
        res.status(400).send(error)
    })
})

GroupController.post("/pay", (req, res) => {
    let groupId = req.body.groupId

        groupBusiness.settleGroup(groupId).then(group => {
            res.send(group)
        }).catch((error) => {
        res.status(400).send(error)
    })
})

export default GroupController;