import express from "express";
import ExpendBusiness from "../Business/ExpendBusiness";
const ExpendController = express.Router();
const expendBusiness = new ExpendBusiness();

ExpendController.post("/", (req, res) => {

    let amount = req.body.amount
    let payId = req.body.payId
    let groupId = req.body.groupId

    expendBusiness.addExpend(amount, payId, groupId).then(expend => {
        res.send(expend)
    }).catch((error) => {
        res.status(400).send(error)
    });
})

// ExpendController.post("/cancel", (req, res) => {
//     let expendId = new ExpendBusiness;

//     if (expendId === null) {
//         throw new Error("Error expend id")
//     expendBusiness.cancelExpend(expendId).then(expend => {
//         res.send(expend)
//     }).catch(error) => {
//     res.status(400).send(error)
// }
// })

export default ExpendController;