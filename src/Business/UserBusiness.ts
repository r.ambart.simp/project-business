import { Group } from "../Entity/Group";
import { User } from "../Entity/User";
import UserRepository from "../Repositories/UserRepository"

export default class UserBusiness {
    private repo: UserRepository = new UserRepository()

    public async findUsers(username: string) {

        let userList: User[] = [];
        let user: User = await this.repo.getUserByName(username).then(user => {
            if (!user) {
                return this.repo.createUser(username);
            }
            return user;
        });

        userList.push(user)
        return userList;
    }


    getUserById(userId: number): Promise<User> {
        return this.repo.getUserById(userId)
    }
    getUserGroups(userId: number): Promise<Group[]> {
        return this.repo.getUserGroups(userId)
    }

}