import { User } from "../Entity/User";
import UserBusiness from "./UserBusiness"
import GroupRepository from "../Repositories/GroupRepository";
import { Group } from "../Entity/Group";
import { UpdateResult } from "typeorm";

export default class GroupBusiness {
    userBusiness: UserBusiness = new UserBusiness();
    groupRepo: GroupRepository = new GroupRepository();

    createGroup(users:string): Promise<Group> {

        return this.userBusiness.findUsers(users).then(users => {
            return this.groupRepo.createGroup(users)
        })
    }

    async addUserToGroup(userId: number, groupId: number) {
        this.userBusiness.getUserById(userId).then((user: User) => {
            return this.groupRepo.addUserToGroup(groupId, user)
        })
    }

    getGroupById(groupId: number): Promise<Group> {
        return this.groupRepo.getGroupById(groupId)
    }

    settleGroup(groupId: number): Promise<UpdateResult> {
        return this.groupRepo.settleGroup(groupId)
    }
}