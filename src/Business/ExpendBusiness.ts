import { Expend } from "../Entity/Expend";
import ExpendRepository from "../Repositories/ExpendRepository";
import GroupBusiness from "./GroupBusiness";
import UserBusiness from "./UserBusiness";

export default class ExpendBusiness {

    private repo: ExpendRepository = new ExpendRepository()
    private userBusiness = new UserBusiness();
    private groupeBusiness = new GroupBusiness();

    async addExpend(amount: number, payId: number, groupId: number): Promise<Expend> {
        let user = await this.userBusiness.getUserById(payId)
        let group = await this.groupeBusiness.getGroupById(groupId)

        return this.repo.addExpend(amount, user, group)
    }

    async cancelExpend(expendExpendId:number) {
        let expend = await this.repo.getExpendById(expendExpendId)
        let group = await this.groupeBusiness.getGroupById(expend.group.id);
        
        return this.repo.cancelExpend(expendExpendId);
        
    }
}