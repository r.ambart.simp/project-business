import express from 'express';
import { createConnection } from 'typeorm';
import routes from './routes/routes'

const app = express();
const port = 8000;


class db {
    static init() {
        return createConnection()
    }
}

db.init().then(() => {
    app.get('/', (req, res) => {
        res.send('Project-Business');
    });

    app.listen(port, () => {
        return console.log(`server is listening on 8000`);
    });

});

app.use("/api", routes);
