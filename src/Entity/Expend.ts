import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { Group } from "./Group";
import { User } from "./User";

@Entity()
export class Expend {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    amount: number;

    @ManyToOne(() => User, user => user.expends)
    pay: User;

    @ManyToOne(() => Group, user => user.expends)
    group: User;
    
    @Column()
    isCancel: boolean;
}
