import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToMany } from "typeorm";
import { Expend } from "./Expend";
import { Group } from "./Group";

Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @OneToMany(() => Expend, expend => expend.pay)
    expends: Expend[];

    @ManyToMany(() => Group, group => group.members)
    groups: Group[];

}
