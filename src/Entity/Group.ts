import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable } from "typeorm";
import { Expend } from "./Expend";
import { User } from "./User";

@Entity()
export class Group {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ default: false })
    isSettled: boolean;

    @Column({ default: false })
    isValidated: boolean;

    @ManyToMany(() => User, user => user.groups)
    members: User[];

    expends: Expend[]

}
